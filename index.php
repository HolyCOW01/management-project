<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>PVCS V2</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <style>
        #space{
            height: 5px;
        }
        body{
            background-color: #ccff99
        }
        #login{
            width: 350px;
            height: 400px;
            background-color: azure;
            position:absolute;
            left:0;
            right:0;
            top:0;
            bottom:0;
            margin:auto;
            border-radius: 5px;
        }
        #l_logo{
            font-family: fantasy;
            font-size: 20px;
        }
        #logo{
            width: 30%;
            height: 30%
        }
        #l_form{
            margin-top: 20px;
        }
        #footer{
            position: absolute;
            left:0;
            right:0;
            bottom:0;
            margin:auto;
            font-size: 10px;
        }
    </style>

    <body>
        <div id="login">
            <div id="l_logo" class="row d-flex justify-content-center">
                <img id="logo" src="Asst/logo BNI 46.png">

                <h1>PVCS Monitoring</h1>
            </div>
            <hr>
            <form action="Model/Connection.php" method="POST" onsubmit="return validasi()">
                <div id="l_form">
                    <div class="d-flex justify-content-center">
                        <input id="username" name="username" placeholder="NPP" type="text" required>
                    </div>
                    <div id="space"></div>
                    <div class="d-flex justify-content-center">
                        <input id="password" name="password" placeholder="Password" type="password" required>
                    </div>
                    <div id="space"></div><div id="space"></div><div id="space"></div>
                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn btn-outline-info">Login</button>
                    </div>
                </div>   
            </form>
            <div id="footer" class="d-flex justify-content-center">
                Powered By BNI Forum
            </div>
        </div>
    </body>
    
</html>
